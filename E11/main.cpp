#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>

using namespace std;

class Vec {
    public:
        Vec(int _len, double *_v);
        Vec(int _len);
        ~Vec();
        Vec(const Vec &other);

        void set(double arg, int coord);
        double get(int coord) const;
        double euc_norm() const;
        double max_norm() const;
        void print() const;

        Vec &operator=(const Vec &vector);
        Vec operator+(const Vec &vector) const;
        Vec operator*(double nb) const;
        bool operator==(const Vec &vector) const;
        double &operator[](unsigned int index);
        double operator[](unsigned int index) const;

        friend ostream &operator<<(ostream &os, const Vec &vector);

    private:
        int len;
        double *v;
};

Vec::Vec(int _len, double *_v)
{
    this->len = _len;
    this->v = new double[_len];
    memcpy(this->v, _v, sizeof(double) * this->len);
}

Vec::Vec(int _len)
{
    this->len = _len;
    this->v = new double[this->len];
    for (int i = 0; i < this->len; i++)
        this->v[i] = 0;
}

Vec::~Vec()
{
    delete[] v;
}

Vec::Vec(const Vec &other)
{
    this->len = other.len;
    this->v = new double[this->len];
    memcpy(this->v, other.v, sizeof(double) * this->len);
}

Vec &Vec::operator=(const Vec &vector)
{
    if (&vector != this) {
        delete[] this->v;
        this->len = vector.len;
        this->v = new double[this->len];
        for (int i = 0; i < this->len; i++)
            this->v[i] = vector.v[i];
    }
    return (*this);
}

void Vec::set(double arg, int coord)
{
    if (coord >= 0 && coord < this->len)
        this->v[coord] = arg;
}

double Vec::get(int coord) const
{
    if (coord >= 0 && coord < this->len)
        return (this->v[coord]);
    return (0);
}

double Vec::euc_norm() const
{
    double _euc_norm = 0;
    for (int i = 0; i < this->len; i++)
        _euc_norm += this->v[i] * this->v[i];
    return (sqrt(_euc_norm));
}

double Vec::max_norm() const
{
    double _euc_norm = this->v[0];
    for (int i = 1; i < this->len; i++)
        _euc_norm = std::max(_euc_norm, std::abs(this->v[i]));
    return (_euc_norm);
}

void Vec::print() const
{
    cout << "(";
    for (int i = 0; i < this->len - 1; i++)
        cout << this->v[i] << ",";
    cout << this->v[len - 1] << ")" << endl;
}

double Vec::operator[](unsigned int index) const
{
    return (this->v[index]);
}

double &Vec::operator[](unsigned int index)
{
    return (this->v[index]);
}

Vec Vec::operator+(const Vec &vector) const
{
    Vec res(this->len);
    for (int i = 0; i < this->len; i++)
        res.v[i] = this->v[i] + vector.v[i];
    return (res);
}

Vec operator*(double nb, const Vec &vector)
{
    return (vector * nb);
}

Vec Vec::operator*(double nb) const
{
    Vec res(this->len);
    for (int i = 0; i < this->len; i++)
        res.v[i] = nb * this->v[i];
    return (res);
}

bool Vec::operator==(const Vec &vector) const
{
    if (this->len != vector.len)
        return (false);
    for (int i = 0; i < vector.len; i++)
        if (vector.v[i] != this->v[i])
            return (false);
    return (true);
}

ostream &operator<<(ostream &os, const Vec &vector)
{
    os << "(";
    for (int i = 0; i < vector.len; i++) {
        os << vector.v[i];
        if (i != vector.len - 1)
            os << ",";
    }
    os << ")";
    return (os);
}