#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <exception>

using namespace std;

class InvalidCoordVecException : public exception {
    string _message;
    int _error1;
    int _error2;

    public:
        InvalidCoordVecException(const char *message, int error1 = 0, int error2 = 0): _message(message), _error1(error1), _error2(error2) {}

        virtual ~InvalidCoordVecException() throw() {}

        const char *what() const noexcept {return (_message.data());};
        int where_1() const {return (_error1);};
        int where_2() const {return (_error2);};
};

class InvalidIndexException : public exception {
    string _message;
    int _error1;

    public:
        InvalidIndexException(const char *message, int error1 = 0): _message(message), _error1(error1) {}

        virtual ~InvalidIndexException() throw() {}

        const char *what() const noexcept {return (_message.data());};
        int where() const {return (_error1);};
};

class Vec {
    public:
        Vec(int _len, double *_v);
        Vec(int _len = 0);
        ~Vec();
        Vec(const Vec &other);

        void set(double arg, int coord);
        double get(int coord) const;
        double euc_norm() const;
        double max_norm() const;
        void print() const;

        Vec &operator=(const Vec &vector);
        Vec operator+(const Vec &vector) const;
        Vec operator*(double nb) const;
        bool operator==(const Vec &vector) const;
        double &operator[](unsigned int index);
        double operator[](unsigned int index) const;

        friend ostream &operator<<(ostream &os, const Vec &vector);

    private:
        int len;
        double *v;
};

Vec::Vec(int _len, double *_v)
{
    this->len = _len;
    this->v = new double[_len];
    memcpy(this->v, _v, sizeof(double) * this->len);
}

Vec::Vec(int _len)
{
    this->len = _len;
    this->v = new double[this->len];
    for (int i = 0; i < this->len; i++)
        this->v[i] = 0;
}

Vec::~Vec()
{
    delete[] v;
}

Vec::Vec(const Vec &other)
{
    this->len = other.len;
    this->v = new double[this->len];
    memcpy(this->v, other.v, sizeof(double) * this->len);
}

Vec &Vec::operator=(const Vec &vector)
{
    if (&vector != this) {
        delete[] this->v;
        this->len = vector.len;
        this->v = new double[this->len];
        for (int i = 0; i < this->len; i++)
            this->v[i] = vector.v[i];
    }
    return (*this);
}

void Vec::set(double arg, int coord)
{
    if (coord >= 0 && coord < this->len)
        this->v[coord] = arg;
}

double Vec::get(int coord) const
{
    if (coord >= 0 && coord < this->len)
        return (this->v[coord]);
    return (0);
}

double Vec::euc_norm() const
{
    double _euc_norm = 0;
    for (int i = 0; i < this->len; i++)
        _euc_norm += this->v[i] * this->v[i];
    return (sqrt(_euc_norm));
}

double Vec::max_norm() const
{
    double _euc_norm = this->v[0];
    for (int i = 1; i < this->len; i++)
        _euc_norm = std::max(_euc_norm, std::abs(this->v[i]));
    return (_euc_norm);
}

void Vec::print() const
{
    cout << "(";
    for (int i = 0; i < this->len - 1; i++)
        cout << this->v[i] << ",";
    cout << this->v[len - 1] << ")" << endl;
}

double Vec::operator[](unsigned int index) const
{
    if (index >= this->len)
        throw InvalidIndexException("incorrect indexing: ", index);
    return (this->v[index]);
}

double &Vec::operator[](unsigned int index)
{
    if (index >= this->len)
        throw InvalidIndexException("incorrect indexing: ", index);
    return (this->v[index]);
}

Vec Vec::operator+(const Vec &vector) const
{
    Vec res(this->len);
    if (vector.len != this->len)
        throw InvalidCoordVecException("addition of vectors of different lengths: ", this->len, vector.len);
    for (int i = 0; i < this->len; i++)
        res.v[i] = this->v[i] + vector.v[i];
    return (res);
}

Vec operator*(double nb, const Vec &vector)
{
    return (vector * nb);
}

Vec Vec::operator*(double nb) const
{
    Vec res(this->len);
    for (int i = 0; i < this->len; i++)
        res.v[i] = nb * this->v[i];
    return (res);
}

bool Vec::operator==(const Vec &vector) const
{
    if (this->len != vector.len)
        return (false);
    for (int i = 0; i < vector.len; i++)
        if (vector.v[i] != this->v[i])
            return (false);
    return (true);
}

ostream &operator<<(ostream &os, const Vec &vector)
{
    os << "(";
    for (int i = 0; i < vector.len; i++) {
        os << vector.v[i];
        if (i != vector.len - 1)
            os << ",";
    }
    os << ")";
    return (os);
}

int main()
{
    try {
        error();
    } catch (InvalidIndexException &e) {
        cerr << "Exception: " << e.what() << e.where();
        return (1);
    } catch (InvalidCoordVecException &e) {
        cerr << "Exception: " << e.what() << e.where_1() << " " << e.where_2();
        return (2);
    } catch (exception &e) {
        cerr << "Exception: unknown error";
        return (3);
    }
    return (0);
}