#include <iostream>
#include <exception>
#include <cstdio>
#include <cstring>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

class Vector_2d;

class NegativeVectorException : public exception {
    string _message;
    int error_1;
    int error_2;

    public:
        NegativeVectorException(const char *arg_comm, int _error_1 = 0, int _error_2 = 0): _message(arg_comm), error_1(_error_1), error_2(_error_2) {}

        virtual ~NegativeVectorException() throw() {}

        const char *what() const noexcept {return (this->_message.data());};
        int where_1() const {return (error_1);};
        int where_2() const {return (error_2);};
};

class OneCoordException : public exception {
    string _message;

    public:
        OneCoordException(const char *arg_comm): _message(arg_comm) {}

        virtual ~OneCoordException() throw() {}

        const char *what() const noexcept {return (_message.data());};
};


class Vector_2d {
    private:
        double x, y;
    public:

        Vector_2d(double arg_x = 0, double arg_y = 0) {
            x = arg_x;        y = arg_y;
        }

        const Vector_2d operator+(const Vector_2d &op);

        void print(const char *inhalt) const;

        double get_x() const {
            return x;
        }
        double get_y() const {
            return y;
        }

        friend const Vector_2d operator*(double a, const Vector_2d &op);

        const Vector_2d operator*(double a) const;

        const Vector_2d &operator+=(const Vector_2d &op);

        const Vector_2d &operator=(const Vector_2d &op);

        const Vector_2d operator-(const Vector_2d &op) const {
            return Vector_2d(x - op.x, y - op.y);
        }

        const Vector_2d &operator++();

        const Vector_2d operator++(int);
        friend istream &operator>>(istream &is, Vector_2d &op);

        friend ostream &operator<<(ostream &os, const Vector_2d &op);

};

istream &operator>>(istream &is, Vector_2d &vector)
{
    int c = 0;
    int i = 0;
    char str[100];

    do {
        c = getchar();
        str[i] = c;
        i++;
    } while (c != '\n');
    for (i = 0; i < 10; i++)
        if (str[i] == '-')
            throw NegativeVectorException("negative coordinate: -", str[i + 1] - 48);
    if (!strchr(str, ' '))
        throw OneCoordException("one coordinate");
    char *token = strtok(str, " ");
    vector.x = std::stoi(token);
    token = strtok(NULL, " ");
    vector.y = std::stoi(token);
    return (is);
}

const Vector_2d &Vector_2d::operator++() {
    x = x + 1.;
    y = y + 1.;
    return *this;
}

ostream &operator<<(ostream &os, const Vector_2d &op) {
    os << "(" << op.x << "," << op.y << ")";
    return os;
}

const Vector_2d Vector_2d::operator++(int) {
    Vector_2d tmp(x, y);
    x = x + 1.;
    y = y + 1.;
    return tmp;
}

const Vector_2d operator*(double a, const Vector_2d &op) {
    return Vector_2d(a * op.x, a * op.y);
}

const Vector_2d Vector_2d::operator*(double a) const {
    return Vector_2d(x * a, y * a);
}

const Vector_2d &Vector_2d::operator+=(const Vector_2d &op) {
    x = x + op.x;
    y = y + op.y;
    return *this;
}

const Vector_2d Vector_2d::operator+(const Vector_2d &op) {
    Vector_2d tmp;
    tmp.x = x + op.x;
    tmp.y = y + op.y;
    return tmp;
}

void Vector_2d::print(const char *inhalt = 0) const {
   if (inhalt){
        cout << inhalt<< " = ";
    }
    cout << "(" << x << "," << y << ")" << endl;
}

const Vector_2d &Vector_2d::operator=(const Vector_2d &op) {
    x = op.x;
    y = op.y;
    return *this;
}

int main()
{
    try {
        Vector_2d v;
        cin >> v;
        cout << v << endl;
    } catch (OneCoordException &e) {
        cerr << "Exception: " << e.what();
        return (1);
    } catch (NegativeVectorException &e) {
        cerr << "Exception: " << e.what() << e.where_1();
        return (2);
    }
    return 0;
}