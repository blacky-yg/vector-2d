#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>

using namespace std;

class Vec {
    public:
        Vec(int _len, double *_v);
        Vec(int _len);
        ~Vec();
        Vec(const Vec &other);

        void set(double arg, int coord);
        double get(int coord) const;
        double euc_norm() const;
        double max_norm() const;
        void print() const;

    private:
        int len;
        double *v;
};

Vec::Vec(int _len, double *_v)
{
    this->len = _len;
    this->v = new double[_len];
    memcpy(this->v, _v, sizeof(double) * this->len);
}

Vec::Vec(int _len)
{
    this->len = _len;
    this->v = new double[this->len];
    for (int i = 0; i < this->len; i++)
        this->v[i] = 0;
}

Vec::~Vec()
{
    delete[] v;
}

Vec::Vec(const Vec &other)
{
    this->len = other.len;
    this->v = new double[this->len];
    memcpy(this->v, other.v, sizeof(double) * this->len);
}

void Vec::set(double arg, int coord)
{
    if (coord >= 0 && coord < this->len)
        this->v[coord] = arg;
}

double Vec::get(int coord) const
{
    if (coord >= 0 && coord < this->len)
        return (this->v[coord]);
    return (0);
}

double Vec::euc_norm() const
{
    double _euc_norm = 0;
    for (int i = 0; i < this->len; i++)
        _euc_norm += this->v[i] * this->v[i];
    return (sqrt(_euc_norm));
}

double Vec::max_norm() const
{
    double _euc_norm = this->v[0];
    for (int i = 1; i < this->len; i++)
        _euc_norm = std::max(_euc_norm, std::abs(this->v[i]));
    return (_euc_norm);
}

void Vec::print() const
{
    cout << "(";
    for (int i = 0; i < this->len - 1; i++)
        cout << this->v[i] << ",";
    cout << this->v[len - 1] << ")" << endl;
}