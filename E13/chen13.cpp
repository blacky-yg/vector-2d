#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <math.h>
#include <cstring>
#include <string>
#include <fstream>
#include <errno.h>
#include <algorithm>
#include <cmath>

using namespace std;

class Vec {
    int len;
    double* v;
public:
	Vec()
	{
	    v = new double[len = 1];
	    v[0] = 0.0;
	}

    Vec(int len, double* values)
    {
    	if (len <= 0)
    	{
    		/*int k = 0;
    		throw k;*/
    		throw "Exception: length error";
    	}
    	this->len = len;
    	v = new double[len];
        memcpy(v, values, len * sizeof(double));
    }
 
    Vec(int len)
    {
    	if (len <= 0)
    	{
    		/*int k = 0;
    		throw k;*/
    		throw "Exception: length error";
    	}
    	this->len = len;
        v = new double[len];
        for (int i = 0; i < len; ++i) 
        {
            v[i] = 0;
        }
    }
 
    Vec(const Vec& other)
    {
    	//this->len = other.len;
    	v = new double[len = other.len]; 
        memcpy(v, other.v, other.len * sizeof(double));
    }
 
    ~Vec() 
    {
        delete[] v;
    }
 
    void set(double arg, int coord) 
    {
        if (coord < len && coord >= 0) 
        {
            v[coord] = arg;
        }
        else
        {
        	/*int k = 2;
        	throw k;*/
        	throw "Exception: coordinate error in set()";
        }
    }
 
    double get(int coord) const 
    {
    	if (coord < len && coord >= 0)
    	{
    		return v[coord];
    	}
    	else
    	{
    		/*int k = 1;
    		throw k;*/
    		throw "Exception: coordinate error in get()";
    	}
    }
 
    double euc_norma() const 
    {
        double norma = 0;
        for (int i = 0; i < len; i++) 
        {
            norma += v[i] * v[i];
        }
        return sqrt(norma);
    }
 
    double max_norma() const 
    {
        double norma = v[0];
        for (int i = 1; i < len; ++i) 
        {
            norma = max(norma, abs(v[i]));
        }
        return norma;
    }
 
    void print() const {
        cout << "(";
        for (int i = 0; i < len - 1; ++i) 
        {
            cout << v[i] << ",";
        }
        cout << v[len - 1] << ")" << endl;
    }
};

/*void error()
{
    double y1[3] = {1,2,3};
    double y2[5] = {2,1,0,5,7};
    Vec x(3, y1), y(5, y2), z(4);
    Vec u = x;
    y.print();
    x.print();
    z.print();
    u.print();
}*/

int main()
{
	try
	{
		error();
	}
	/*catch(int k)
	{
		if (k == 0)
		{
			cerr << "Exception: length error" << endl;
			return 0;
		}
		if (k == 1)
		{
			cerr << "Exception: coordinate error in get()" << endl;
			return 0;
		}
		if (k == 2)
		{
			cerr << "Exception: coordinate error in set()" << endl;
			return 0;
		}
	}*/
	catch(const char * err)
	{
		cerr << err << endl;
	}
  	catch(...)
	{
		cerr << "Exception: unknown error" << endl;
	}
	return 0;
}

/*int main(void)
{
	try
	{
	    double y1[3] = {1,2,3};
	    double y2[5] = {2,1,0,5,7};
	    Vec x(3, y1), y(5, y2), z(4);
	    Vec u = x;

	    y.print();
	    x.print();
	    z.print();
	    u.print();

	    x.set(23, 2);  u.set(34, 1);  z.set(-3, 3);
	    cout << "x[2] = " << x.get(2) << endl;
	    cout << "u[1] = " << u.get(1) << endl;
	    cout << "euc_norma y: " << y.euc_norma() << endl;
	    cout << "max_norma z: " << z.max_norma() << endl;
	}
	catch(int k)
	{
		cout << "cool" << endl;
	}
    return 0;
}*/